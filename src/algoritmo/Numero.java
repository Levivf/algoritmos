/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmo;

/**
 *
 * @author Levi
 */
public class Numero {
    int num;
    public void setNum (int num) {
        this.num = num;
    }
    
    public int getNum() {
        return num;
    }
    
    public int sumaDigitos() {
        int numAux=num;
        int suma=0;
        int residuo;
        while (numAux>0) {
            residuo=numAux%10;
            suma =suma+residuo;
            numAux=numAux/10;
        }
        return suma;
    }
    public static void main (String[] args) {
        Numero n=new Numero();
        n.setNum(1234);
        System.out.println("suma: "+n.sumaDigitos());
        
    }
}
